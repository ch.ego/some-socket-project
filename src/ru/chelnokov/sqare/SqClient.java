package ru.chelnokov.sqare;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * Работа по протоколу TCP
 * Клиент отправляет байт (число) серверу,
 * затем получает и выводит квадрат числа
 */
public class SqClient {
    public static void main(String[] args)  {
        Scanner reader = new Scanner(System.in);
        try(Socket socket = new Socket("localhost", 8081)){
            OutputStream outputStream = socket.getOutputStream();
            while (true){
                outputStream.write(reader.nextInt());
                outputStream.flush();
                InputStream inputStream = socket.getInputStream();
                int response = inputStream.read();
                System.out.println(response);
                if (response == 0){
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}